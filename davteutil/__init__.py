"""Useful functions used by Davte when programming in python."""

__author__ = "Davide Testa"
__email__ = "davte@libero.it"
__credits__ = ""
__license__ = "GNU General Public License v3.0"
__version__ = "1.1.17"
__maintainer__ = "Davide Testa"
__contact__ = "t.me/davte"

__all__ = [
    'utilities',
]
